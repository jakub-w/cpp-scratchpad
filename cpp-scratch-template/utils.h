#ifndef CPP_SCRATCHPAD_UTILS_H
#define CPP_SCRATCHPAD_UTILS_H

#include <chrono>
#include <functional>
#include <iostream>

namespace cpps {
template<typename Container>
static std::string to_hex(Container c) {
  const unsigned char* buf =
      reinterpret_cast<const unsigned char*>(std::data(c));
  const auto size = std::size(c);
  const auto value_size = sizeof(typename decltype(c)::value_type);

  auto output = std::string(size * value_size * 2, ' ');

  for (size_t i = 0; i < size; ++i) {
    snprintf(output.data() + (i * value_size * 2), 3, "%02x", buf[i]);
  }

  return output;
}

// Print buffer as a hex string (to stdout).
template <typename T>
static void print_hex(T* buffer, size_t size) {
  auto* int_buffer = reinterpret_cast<const unsigned char*>(buffer);
  for (size_t i = 0; i < size * sizeof(T); ++i) {
    char s[3];
    snprintf(s, 3, "%02x", int_buffer[i]);
    std::cout.write(s, 2);
  }
  std::cout << '\n';
}

template <typename T>
static void print_hex(T container) {
  print_hex(std::begin(container), std::size(container));
}

// Count the time it takes the function f to complete (in milliseconds).
template <typename Func, typename ...Args>
static void time(Func&& f, Args&&... args) {
  auto start = std::chrono::high_resolution_clock::now();
  std::invoke(std::forward<Func>(f), std::forward<Args>(args)...);
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::ratio<1, 1000>> duration = (end - start);
  std::cout << "Time elapsed: " << duration.count() << " ms\n";
}
}

#endif // CPP_SCRATCHPAD_UTILS_H
